
class BodyValidator < ActiveModel::Validator
  def validate(record)
    if record.body.include? 'Пыщь'
      record.errors.add :body, "Пышь писать нельзя!"
    end
  end
end