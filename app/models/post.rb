class Post < ApplicationRecord
  scope :max_likes, -> { Post.all.order(likes_count: :DESC).first(3) }

  has_many_attached :images, dependent: :destroy

  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy

  belongs_to :user

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 10 }
end
