class PostsController < ApplicationController

  def index
    @posts = Post.includes(:user).includes(:likes).all.order(created_at: :desc)
    #@top_posts = @posts.sort { |post, other_post| other_post.likes_count <=> post.likes_count } [0..2]
    @top_posts = Post.max_likes
    @posts = @posts.page.per(6)
    authorize @posts

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: { posts: @posts, top_posts: @top_posts } }
    end
  end

  def show
    @post = Post.includes(:user).includes(:likes).find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @post }
    end
  end

  def new
    @post = Post.new
    authorize @post
  end

  def create
    @post = current_user.posts.new(post_params)
    authorize @post

    if @post.save
      redirect_to @post
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @post = Post.find(params[:id])
    authorize @post
  end

  def update
    @post = Post.find(params[:id])
    authorize @post
    if @post.update(post_params)
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    @post = Post.find(params[:id])
    authorize @post
    @post.destroy
    
    redirect_to root_path, status: :see_other
  end

  private

  def post_params
    params.require(:post).permit(:title, :body, images: [])
  end
end
