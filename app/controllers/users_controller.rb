class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    @user = User.includes(:posts).includes(:follows).find(params[:id])

    @following_posts = Post.includes(:user).where('user_id' => @user.all_following)
  end
end
