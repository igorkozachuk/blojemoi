class RegistrationsController < Devise::RegistrationsController
  
  def create
    @user = User.create!(user_params)
    session[:user_id] = @user.id

    # UserMailer.with(user: @user).welcome_email.deliver_now
    redirect_to root_path
  
  end
  
  private

  def user_params
    params.require(:user).permit(:email, :username, :password, :password_confirmation, :avatar)
  end

  def after_sign_up_path_for(_resource)
    root_path
  end
end
