Rails.application.routes.draw do
  authenticate :user, ->(user) { user.role == 'admin' } do
    mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  end

  get '/admin' => redirect('/')

  devise_for :users
  
  root 'posts#index'

  resources :users

  #followings
  get '/follows/:id', to: 'follows#show', as: 'follows'
  post '/follow/:id', to: 'follows#create', as: 'follow'
  delete '/unfollow/:id', to: 'follows#destroy', as: 'unfollow'

  resources :posts do
    resources :likes, only: %i[create destroy]
    resources :comments, only: %i[create destroy]
  end

end
