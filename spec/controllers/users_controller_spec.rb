require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:user) { create(:user) }
  before { sign_in user }

  describe '#index' do
    subject { process :index }
    let(:users) { create_list(:user, 3) }

    it 'renders the index template' do
      subject
      expect(response).to render_template :index
    end

    it 'redirects to the sign in page' do
      sign_out user
      subject
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe '#show' do
    subject { process :show, params: { id: user.id } }

    it 'renders the show template' do
      subject
      expect(response).to render_template :show
    end

    it 'redirects to the sign in page' do
      sign_out user
      subject
      expect(response).to redirect_to new_user_session_path
    end
  end
end
