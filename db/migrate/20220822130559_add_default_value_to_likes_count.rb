class AddDefaultValueToLikesCount < ActiveRecord::Migration[7.0]
  def change
    change_column_null :posts, :likes_count, false
    change_column_default :posts, :likes_count, 0
  end
end
